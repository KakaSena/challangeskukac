# A Small Report

Songstream is a music streaming service. They pay artists
when people stream their music, but they need to know exactly
_who_ is going to be paid, and how much.

For that, they have a service that processes an incoming
stream of "anonimised listening events", and figure out
how much to pay each artist.

A prototype of the service was implemented by a previous
developer, but that person is not in the reports team
anymore. They have, however, left some documentation
for the system.

> This system processes a stream of anonimised listening
> events from all users in a particular month to find
> how much we should pay each artist.
>
> Each song someone listened to grants the artist $0.09.
> But songs may be performed by more than one artist. In such
> cases the main artist gets $0.09, while each of the guests
> gets \$0.01.
>
> Each event in the stream has the following shape:
>
>     {
>       "artist_id": "unique-id",
>       "guest_ids": ["unique-id", "unique-id"],
>       "listened": 100, // in seconds
>       "duration": 270, // in seconds
>       "date": "2018-10-22",
>       "song_id": "unique-id"
>     }
>
> The `calculateRevenues` function takes in the stream of
> events, and returns a list of artists with their revenue.
> To avoid problems with precision, the revenue is returned
> as an integer by moving the decimal comma 3 places right.
> That is, `$ 10.30` becomes the number `10300`.

While the code works for the original requirements, it's not very
efficient, and it could be improved for maintainability.

Not only that, but the higher-ups at Songstream decided to change
these requirements a little bit:

- Artists now take a bonus if people listen to them for a certain
  amount of minutes in one day. If people listen to them more than
  4 hours in one day, they get 1% more revenue. 8 hours+ grants
  them 2% more revenue. And 16 hours+ grants them 3% more revenue.

- Songs that have been listened to for less than 10 seconds
  do not count towards the artists revenue, or their bonuses.

As the new programmer in the reports team at Songstream, they're
counting on you to implement these new features in the existing
codebase.

## Working with this repository

You should implement your solution in the `source/` folder. The `index.ts` file
already includes some pointers on where you can start.

The first thing you should do is installing the project's dependencies with
`npm install`.

At any moment you can run the tests with `npm test`.

Once the tests pass, and you're happy with the solution, you can submit it.
