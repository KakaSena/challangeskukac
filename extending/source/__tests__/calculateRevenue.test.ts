import { calculateRevenues } from "../index";
import "jest";

describe(`calculateRevenues(events)`, () => {
  const listen = (
    date: string,
    artist: string,
    guests: string[],
    time: number
  ) => ({
    artist_id: artist,
    guest_ids: guests,
    date: date,
    duration: time,
    listened: time,
    song_id: "a"
  });

  const songs = (
    n: number,
    date: string,
    artist: string,
    guests: string[],
    time: number
  ) => Array.from({ length: n }, () => listen(date, artist, guests, time));

  const byArtist = (a: any, b: any) => a.artist_id.localeCompare(b.artist_id);

  test(`Calculate revenues for main and guest artists`, () => {
    expect(
      calculateRevenues([
        listen("2018-10-01", "a", [], 100),
        listen("2018-10-01", "a", ["b", "c"], 100),
        listen("2018-10-01", "a", ["c"], 100)
      ]).sort(byArtist)
    ).toEqual([
      {
        artist_id: "a",
        revenue: 90 * 3
      },
      {
        artist_id: "b",
        revenue: 10
      },
      {
        artist_id: "c",
        revenue: 20
      }
    ]);
  });

  describe(`Get a bonus if listen to N minutes a day`, () => {
    test(`4 hours+`, () => {
      expect(
        calculateRevenues([
          ...songs(100, "2018-10-01", "a", ["b"], 180),
          listen("2018-10-02", "a", [], 100)
        ]).sort(byArtist)
      ).toEqual([
        { artist_id: "a", revenue: 90 + 90 * 100 + 90 },
        { artist_id: "b", revenue: 10 * 100 + 10 }
      ]);
    });

    test(`8 hours+`, () => {
      expect(
        calculateRevenues([
          ...songs(100, "2018-10-01", "a", ["b"], 360),
          listen("2018-10-02", "a", [], 100)
        ]).sort(byArtist)
      ).toEqual([
        { artist_id: "a", revenue: 90 + 90 * 100 + 180 },
        { artist_id: "b", revenue: 10 * 100 + 20 }
      ]);
    });

    test(`16 hours+`, () => {
      expect(
        calculateRevenues([
          ...songs(100, "2018-10-01", "a", ["b"], 720),
          listen("2018-10-02", "a", [], 100)
        ]).sort(byArtist)
      ).toEqual([
        { artist_id: "a", revenue: 90 + 90 * 100 + 270 },
        { artist_id: "b", revenue: 10 * 100 + 30 }
      ]);
    });
  });

  test(`Ignore songs listened for <10 seconds`, () => {
    expect(
      calculateRevenues([
        listen("2018-10-01", "a", ["b"], 5),
        listen("2018-10-01", "a", [], 100)
      ])
    ).toEqual([{ artist_id: "a", revenue: 90 }]);
  });
});
