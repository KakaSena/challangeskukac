
interface Event{
  artist_id: string;
  guest_ids: string[];
  song_id: string;
  date: string;
  listened: number;
  duration: number;
}

interface Revenue {
  artist_id: string;
  revenue: number;
}

const REVENUE_PER_SONG = 90;
const REVENUE_PER_GUEST = 10;

/**aaaaaaaaaaa
 *  This function will implement the new features.
 * If songs were listened for less than 10 seconds, it does not coun't revenue or bonus
 * Bonus: 
 *    song is listened more than 4 hours they get 1% more revenue.  (add 90)
 *    song is listened more than 8 hours they get 2% more revenue.  (add 180)
 *    song is listened more than 16 hours they get 3% more revenue. (add 270)
 */

// function bonus(revenues: Revenue[], events: Event[], artist: string, revenue: number, data: Map<string, Map<string, number>>, artistMap: Map<string, number>) {
//   let Ttime = 0
//     for(let event of events){
//       for(let item of revenues){
//         data.get(artist)
//         Ttime += event.listened
//         if(Ttime >= 14400 && Ttime <28800 ){
//             item.revenue = 69
//             return
//         }
//         else if (Ttime >= 28800 && Ttime < 57600){
//             item.revenue = 42
//             return
//         }
//         else if (Ttime >= 57600){
//             item.revenue = 51
//             return
//         }
//       }
//     }
//     revenues.push({ artist_id: artist, revenue: revenue });
//   }
  
function bonus(revenues: Revenue[], events: Event[], artist: string, revenue: number, data: Map<string, Map<string, number>>, artistMap: Map<string, number>) {
  let Ttime = 0
    for(let event of events){
      for(let item of revenues){
        if(item.artist_id === artist){
          item.revenue += revenue;
          return;
        }
      }
    }
    revenues.push({ artist_id: artist, revenue: revenue });
  }

// function insertRevenue(revenues: Revenue[], artist: string, revenue: number) {
//   for (const item of revenues) {
//     if (item.artist_id === artist){
//       item.revenue += revenue;
//       return;
//     }
//   }
//   revenues.push({ artist_id: artist, revenue: revenue});
// }

/**
 * Processes a stream of anonimised listening events
 * from all users in a particular month to find out
 * how much we should pay each artist.
 */
export function calculateRevenues(events: Event[]): Revenue[] {
  let revenues: Revenue[] = [];
  let dateMap = new Map<string, Map<string, number>>();
  let artistMap = new Map<string, number>()

  for (const event of events){
    for(const rev of revenues)
      artistMap.set(event.artist_id, rev.revenue)
      dateMap.set(event.date, artistMap)
  
     
      if(event.duration > 10){
        bonus(revenues, events, event.artist_id, REVENUE_PER_SONG, dateMap, artistMap)
        //insertRevenue(revenues, event.artist_id, REVENUE_PER_SONG);
        for(const guest of event.guest_ids){
          bonus(revenues, events, guest, REVENUE_PER_GUEST, dateMap, artistMap)
          //insertRevenue(revenues, guest, REVENUE_PER_GUEST);
        }
      } 
  }
  //console.log(dateMap)
  return revenues
}

  // dateMap.get(event.date).set(rev.artist_id, rev.revenue)


      //artistMap.set(event.artist_id, rev.revenue)
      //dateMap.set(event.date, artistMap)

    //   for(event of events) {
    //     data.set(event.date, new Map())
    //     for(item of revenues) {
    //         data.get(event.date).set(item.artist_id, revenues.revenue)
    //     }
    // }
    
    
    //   for(event of events) {
    //     data.set(event.date, new Map())
    //     for(item of revenues) {
    //         artistMap.set(item.artist_id, revenues.revenue)
    //         data.get(event.date).set(artistMap)
    //     }
    // }
