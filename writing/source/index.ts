/**
 * A flat representation of a role.
 */
export interface Role {
  id: string;            // unique ID for the role
  name: string;          // friendly name for the role
  parent: string | null; // either the parent's ID or null
}

/**
 * A function to create a tree from a flat array.
 * The array has a id, the name for the id and it's parent.
 */
export function unflatten(){}


/**
 * Available permissions.
 */
export type Permission = "read" | "write" | "list";

/**
 * Represents a file, together with its permissions.
 */
export interface File {
  uuid: string;                  // unique ID for the file
  title: string;                 // friendly name for the file
  permissions: FilePermission[];
}

/**
 * Represents permissions for a particular role.
 */
export interface FilePermission {
  role: string;          // ID of the role
  allowed: Permission[]; // List of permissions they have
}

/**
 * True if the `role` is allowed to perform the `action`
 * on the given `file`.
 */
export function isAllowed(
  groupRoles: Role[], // the list of roles in the group
  role: string,       // the role that's trying to perform an action
  action: Permission, // the permission that we're trying to check
  file: File          // the file that they're trying to act on
): boolean {

  
  throw new Error(`I did not exist yet...`);
}
