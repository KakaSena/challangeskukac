import { isAllowed } from "../index";
import "jest";

test(`isAllowed should succeed if permission is directly given to role`, () => {
  expect(
    isAllowed(
      [{ id: "everyone", name: "Everyone", parent: null }],
      "everyone",
      "read",
      {
        uuid: "file",
        title: "File",
        permissions: [{ role: "everyone", allowed: ["read"] }]
      }
    )
  ).toBe(true);
});

test(`isAllowed should fail if the role doesn't exist`, () => {
  expect(
    isAllowed(
      [{ id: "readers", name: "Readers", parent: null }],
      "everyone",
      "read",
      {
        uuid: "file",
        title: "File",
        permissions: [{ role: "readers", allowed: ["read"] }]
      }
    )
  ).toBe(false);
});

test(`isAllowed should fail if there's no such permission for the role`, () => {
  expect(
    isAllowed(
      [{ id: "readers", name: "Readers", parent: null }],
      "readers",
      "write",
      {
        uuid: "file",
        title: "File",
        permissions: [{ role: "readers", allowed: ["read"] }]
      }
    )
  ).toBe(false);
});

test(`isAllowed should succeed if the permission is indirectly defined for the role`, () => {
  expect(
    isAllowed(
      [
        { id: "everyone", name: "Everyone", parent: null },
        { id: "readers", name: "Readers", parent: "everyone" }
      ],
      "readers",
      "read",
      {
        uuid: "file",
        title: "File",
        permissions: [{ role: "everyone", allowed: ["read"] }]
      }
    )
  ).toBe(true);
});
