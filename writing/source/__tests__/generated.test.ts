import { Role, isAllowed, Permission, FilePermission } from "../index";
import * as fc from "fast-check";

class Unique {
  private counter = 0;

  next() {
    return ++this.counter;
  }
}

interface IRoleTree {
  id: string;
  children: IRoleTree[];
}

const APermission: fc.Arbitrary<Permission> = fc.oneof(
  fc.constant("read"),
  fc.constant("write"),
  fc.constant("list")
) as any;

function ARoleTree(maxDepth: number): fc.Arbitrary<IRoleTree> {
  function reIdentify(node: IRoleTree, unique: Unique): IRoleTree {
    const id = String(unique.next());

    return {
      id: id,
      children: node.children.map(node => reIdentify(node, unique))
    };
  }

  function go(maxDepth: number): fc.Arbitrary<IRoleTree> {
    if (maxDepth <= 0) {
      return fc.record({
        id: fc.constant(""),
        children: fc.constant([])
      });
    } else {
      return fc.record({
        id: fc.constant(""),
        children: fc.array(go(maxDepth - 1), maxDepth)
      });
    }
  }

  return go(maxDepth).map(node => reIdentify(node, new Unique()));
}

function flatMap<A, B>(xs: A[], f: (_: A) => B[]): B[] {
  return xs.map(f).reduce((a, b) => a.concat(b), []);
}

function flattenTree(tree: IRoleTree) {
  function unfold(node: IRoleTree, parent: null | string): Role[] {
    return [
      { id: node.id, name: node.id, parent: parent },
      ...flatMap(node.children, x => unfold(x, node.id))
    ];
  }

  return unfold(tree, null);
}

type RoleInfo = {
  id: string;
  depth: number;
  parent: string | null;
};

function allRoles(tree: IRoleTree): RoleInfo[] {
  function go(
    tree: IRoleTree,
    parent: string | null,
    depth: number
  ): RoleInfo[] {
    return [
      {
        id: tree.id,
        depth,
        parent
      }
    ].concat(flatMap(tree.children, t => go(t, tree.id, depth + 1)));
  }

  return go(tree, null, 0);
}

function shuffle<A>(array: A[]): A[] {
  return array.sort(() => 0.5 - Math.random());
}

test(`[fuzzy] isAllowed(roles, role, action, file)`, () => {
  fc.assert(
    fc.property(
      ARoleTree(4),
      APermission,
      fc.array(APermission, 10),
      (tree, action, perms) => {
        const roles = shuffle(allRoles(tree));
        const roleInput = flattenTree(tree);
        const role = roles[0];

        const roleMap = roleInput.reduce((m, r) => {
          m.set(r.id, r);
          return m;
        }, new Map());

        const filePerms = [];
        const ps = perms.slice();
        let r = role;
        while (ps.length > 0) {
          if (r.parent == null) {
            filePerms.push({ role: r.id, allowed: ps });
            break;
          } else {
            const perm = ps.pop();
            if (perm != null) {
              filePerms.push({ role: r.id, allowed: [perm] });
              r = roleMap.get(r.parent);
            } else {
              break;
            }
          }
        }

        return (
          isAllowed(roleInput, role.id, action, {
            uuid: "file",
            title: "file",
            permissions: filePerms
          }) === perms.includes(action)
        );
      }
    )
  );
});
