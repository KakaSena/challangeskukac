# Can They Manipulate That File?

Datahouse Co. provides a service where they allow files to
be stored by many people in a shared cloud. These people
are granted "roles" in this shared storage, and roles
describe what can be done with the files.

Because they wanted to simplify the life of system administrators,
they came up with a concept of "role hierarchies". This means that
each role may "inherit" some permissions from another role.

For example, if the role `auditors` inherits from `everyone`, and
`everyone` has been granted the power to read some file, surely
`auditors` also must have that power.

Effectively, this means that roles form a tree, and that permissions
propagate down the tree. Datahouse Co. wanted users to have a lot of
flexibility with these roles, so they decided that users should be
able to specify roles and their hierarchy. This is how they look in
the database:

```json
[
  { "id": "everyone", "name": "Everyone", "parent": null },
  { "id": "auditors", "name": "Auditors", "parent": "everyone" },
  { "id": "editors", "name": "Editors", "parent": "everyone" },
  { "id": "guests", "name": "Guests", "parent": "everyone" },
  { "id": "writers", "name": "Writers", "parent": "editors" }
]
```

Somehow they've managed to guarantee that items with a parent will always
come after an item with that ID in the list. Although you're sceptical of
their choice of representation none the less.

This can be, more clearly, be seen as the following hierarchical tree:

```
                    Everyone
                    /  |  \
                   /   |   \
                  /    |    \
                 /     |     \
                v      v      v
          Auditors   Guests  Editors
                              |
                              v
                             Writers
```

This helps seeing that `Editors` can also do anything `Everyone` can do, but
`Writers` can also do anything `Editors` or `Everyone` can do.

Permissions are stored with the files. To keep things simple, Datahouse Co.
decided to represent this file metadata as JSON. Each file contains its
unique id, its friendly name, and its permissions. Permissions are defined
as an array of what each role can do. For example:

```
{
  "uuid": "VhzU29gyQDJdwoSnVlGyEA",
  "title": "Meeting Minutes 24/12",
  "permissions": [
    { "role": "everyone", "allowed": ["list"] },
    { "role": "guests", "allowed": ["read"] },
    { "role": "editors", "allowed": ["read"] }
    { "role": "writers", "allowed": ["write"] }
  ]
}
```

The valid permissions are:

- `read`, which allows one to see the contents of the file;
- `write`, which allows one to modify the contents of the file; and
- `list`, which allows one to see the meta-data of the file, but nothing else.

So in this case everyone would be able to see the metadata of the file;
`guests` and `editors` would be able to additionally see the contents of
the file, but not modify them; and `writers` would be able to see the
contents of the file and modify it.

Now Datahouse Co. needs to implement the actual access control mechanisms
for this idea. As a new programmer joining Datahouse Co., you've been
tasked with taking the idea from paper to an actual implementation
that can run on Datahouse Co.'s file sharing service.

The engineer that asked you to implement the idea has included a function
signature that they expect you to implement:

```ts
function isAllowed(
  groupRoles: Role[], // the list of roles in the group
  role: string, // the role that's trying to perform an action
  action: Permission, // the permission that we're trying to check
  file: File // the file that they're trying to act on
): boolean;
```

Helpfully, they've also included signatures for the other data structures
that your function will receive:

```ts
interface Role {
  id: string;         // unique ID for the role
  name: string;       // friendly name for the role
  parent: string?     // either the parent's ID or null
}

enum Permission {
  read,               // can read the file
  write,              // can write the file
  list                // can see the meta-data
}

interface File {
  uuid: string,       // unique ID for the file
  title: string,      // friendly name for the file
  permissions: FilePermission[]
}

interface FilePermission {
  role: string;           // ID of the role
  allowed: Permission[]   // List of permissions they have
}
```

Datahouse Co. uses a continuous delivery system, so all features are
expected to include automated tests--the continuous integration system
uses them to verify if something looks safe enough to send to production.
Because the feature will be deployed directly to production, Datahouse Co.
also expects code that is production-ready: reasonably robust, well
organised, and minimally documented.

You'll see that the person who tasked you with this feature has already
written some tests for you. But, of course, you might need to write your
own automated tests too.
