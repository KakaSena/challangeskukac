# Challenges proposed by Quildreen Motta.

Programming challenge divided in two parts.

1. The candidate is presented with a problem description, and
should write a suitable solution for it.

2. The second one, the candidate is presented with an existing piece of code,
and a new problem description. They should then modify the program so it
solves the new problem.

Each folder has a "question.md" file that explains the problem.